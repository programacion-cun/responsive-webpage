import { Component, OnInit } from '@angular/core';
import {ListItems} from "../list-items.object";

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  public itemsToList: number = 0;
  public itemsList: Array<ListItems> = [];
  constructor() { }

  ngOnInit(): void {
    this.itemsToList = 10;
    this.populateItems();
  }
  populateItems(): void{
    for (let i = 0; i < this.itemsToList; i ++){
      this.itemsList.push({
        title: 'Pijama ' + (i+1),
        image: 'image-' + i
      })
    }
  }

}
